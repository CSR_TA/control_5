#include <stdio.h>
#include <stdlib.h>
#include "estudiantes.h"
//Se incluye la libre math para luego usarlo en la desviacion estandar
#include <math.h>


float desvStd(estudiante curso[]){
	return 0.0;
}

float menor(float prom[]){
	return 0.0;
}

float mayor(float prom[]){
	return 0.0;
}

void registroCurso(estudiante curso[]){
	//debe registrar las calificaciones 
	int i;
	float nota_proyecto, nota_control, prom;
	/*i<24*/
	for (i=0;i<24;i++){
//Se muestra el nombre del alumno en pantalla
		printf("%s %s %s\n", curso[i].nombre, curso[i].apellidoP, curso[i].apellidoM);
//Se capturan las notas
		printf("Ingrese nota proyecto 1: \n");
		scanf("%f",&curso[i].asig_1.proy1);
		printf("Ingrese nota proyecto 2: \n");
		scanf("%f",&curso[i].asig_1.proy2);
		printf("Ingrese nota proyecto 3: \n");
		scanf("%f",&curso[i].asig_1.proy3);
		printf("Ingrese nota control 1: \n");
		scanf("%f",&curso[i].asig_1.cont1);
		printf("Ingrese nota control 2: \n");
		scanf("%f",&curso[i].asig_1.cont2);
		printf("Ingrese nota control 3: \n");
		scanf("%f",&curso[i].asig_1.cont3);
		printf("Ingrese nota control 4: \n");
		scanf("%f",&curso[i].asig_1.cont4);
		printf("Ingrese nota control 5: \n");
		scanf("%f",&curso[i].asig_1.cont5);
		printf("Ingrese nota control 6: \n");
		scanf("%f",&curso[i].asig_1.cont6);
//Se calcula el valor de las notas del proyecto las cuales al sumarlas da un valor del 70% 
		nota_proyecto= (curso[i].asig_1.proy1*0.2)+(curso[i].asig_1.proy2*0.2)+(curso[i].asig_1.proy3*0.3);
//Se calcula el valor de las notas de control las cuales al sumarlas da un valor del 30% 
		nota_control = (curso[i].asig_1.cont1*0.05)+(curso[i].asig_1.cont2*0.05)+(curso[i].asig_1.cont3*0.05)+(curso[i].asig_1.cont4*0.05)+(curso[i].asig_1.cont5*0.05)+(curso[i].asig_1.cont6*0.05);
//Se  suman las notas del control y proyecto, el cal da un 100%
		curso[i].prom = nota_proyecto+nota_control;
	}
}

void clasificarEstudiantes(char path[], estudiante curso[]){
	printf("TEST"); 
}


void metricasEstudiantes(estudiante curso[]){
//Se definen las variables enteras
	int menor_alumno, mayor_alumno, i, suma=0;
//Se definen variables flotantes o decimales
	float menor = 8.0, mayor = 0.0, desv_estandar=0.0, media;
/*Se recorre la lista de alumnos para 
mostrar luego en pantalla el con mejor promedio, el mas
bajo promedio y la desviacion estandar*/
	for (i=0;i<24;i++){
		if(curso[i].prom <= menor){
			menor = curso[i].prom;
			menor_alumno = i;
		}
		if(curso[i].prom >= mayor){
			mayor = curso[i].prom;
			mayor_alumno = i;
		}
		suma+=curso[i].prom;
	}
		media=(suma/3);
		for (i=0;i<24;i++){
			desv_estandar+=(curso[i].prom-media)*(curso[i].prom-media);
		}
/*Se usa la funcion pow para calcular el exponente
pero en este caso lo uso para calcular raiz de dos*/
		desv_estandar=pow((desv_estandar/3),(1/2));
//Se imprime la informacion del alumno menor, mayor y desviacion estandar
		printf("El estudiante con el mas bajo promedio es: %s %s %s\n", curso[menor_alumno].nombre,curso[menor_alumno].apellidoP, curso[menor_alumno].apellidoM);
		printf("El estudiante con el mas alto promedio es: %s %s %s\n", curso[mayor_alumno].nombre,curso[mayor_alumno].apellidoP, curso[mayor_alumno].apellidoM);
		printf("la desviación estandar es de: %f\n", desv_estandar);
}


void menu(estudiante curso[]){
	int opcion;
    do{
		printf( "\n   1. Cargar Estudiantes" );
		printf( "\n   2. Ingresar notas" );
		printf( "\n   3. Mostrar Promedios" );
		printf( "\n   4. Almacenar en archivo" );
		printf( "\n   5. Clasificar Estudiantes " );
		printf( "\n   6. Salir." );
		printf( "\n\n   Introduzca opción (1-6): " );
		scanf( "%d", &opcion );
        switch ( opcion ){
			case 1: cargarEstudiantes("estudiantes.txt", curso); //carga en lote una lista en un arreglo de registros
					break;

			case 2:  registroCurso(curso);// Realiza ingreso de notas en el registro curso 
					break;

			case 3: metricasEstudiantes(curso); //presenta métricas de disperción, tales como mejor promedio; más bajo; desviación estándard.
					break;

			case 4: grabarEstudiantes("test.txt",curso);
					break;
            case 5: clasificarEstudiantes("destino", curso); // clasi
            		break;
         }

    } while ( opcion != 6 );
}

int main(){
	estudiante curso[30]; 
	menu(curso); 
	return EXIT_SUCCESS; 
}